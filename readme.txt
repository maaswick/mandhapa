=== Mandhapa ===
Contributors: Distrapps
Tags: simple, company profile, hotel, resort
Requires at least: 4.8
Stable tag: v1.0.1
Version: 1.0.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html


== Description ==
Mandhapa is a Wordpress theme designed specifially for hotel, resort and villa.


== Installation ==
This section describes how to install the child theme and get it working.

== Changelog ==
= 1.0.1 =
* Styling contact form
* Add fonts in function
* Adjust some page

= 1.0.0 =
* initial files
