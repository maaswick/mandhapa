<?php

/**
 * Template Name: Room Details
 */

get_header();

?>

<style media="all">
  #slider-main.owl-carousel .image-slide {
    padding-bottom: 60vh;
  }
</style>

<div id="content" class="page">

  <div class="page-intro">
    <?php if( get_field('room_cover_image') ): ?>
      <div class="intro-inner" style="background-image: url('<?php the_field('room_cover_image'); ?>')">
	  <?php endif; ?>
    </div><!-- end .intro-inner -->
  </div><!-- end .page-intro -->

  <div class="content-wrap">
    <section class="section-introduce">
      <div class="container">
        <div class="heading-section heading-padspace text-center">
            <h2>Deluxe Room</h2>
            <?php the_field('room_intro'); ?>
        </div>

        <div class="row">

          <?php
            $images = get_field('room_images');
            if( $images ): ?>

              <div id="slider-main" class="owl-carousel">
                <?php foreach( $images as $image ): ?>
                  <div class="owl-slide">
                    <div class="image-slide" style="background-image: url('<?php echo $image['url']; ?>')"></div>
                  </div>
                <?php endforeach; ?>
              </div><!-- end .slider-main -->

          <?php endif; ?>

        </div><!-- end .row -->
      </div><!-- end .container -->
    </section><!-- end .section-introduce -->

    <section class="section-description">
      <div class="container">
        <?php the_field('room_description'); ?>
      </div><!-- end .container -->
    </section><!-- end .section-description -->
  </div><!-- end .content-wrap -->

</div><!-- end #content -->

<?php get_footer(); ?>
